package com.galvanize.helloSpring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@RestController
public class VowelController {

    @GetMapping("/vowels")
    public int vowelCount(@RequestParam String word){
        int vowelCount = 0;
        Set<Character> vowels = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u'));

        for (int i = 0; i < word.length(); i++) {
            if (vowels.contains(word.charAt(i))){
                vowelCount++;
            }
        }

        return vowelCount;
    }
}
