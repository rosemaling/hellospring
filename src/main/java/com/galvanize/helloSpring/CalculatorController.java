package com.galvanize.helloSpring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {

    @GetMapping("/calculator")
    public int calculate(@RequestParam int[] values,
                         @RequestParam String operand){
        int result = 0;

        switch(operand){
            case "+":
                for (int value : values) {
                    result += value;
                }
                break;
            case "-":
                result = values[0];
                for (int i = 1; i < values.length; i++){
                    result -= values[i];
                }
                break;
            case "*":
                result = 1;
                for (int value : values) {
                    result *= value;
                }
                break;
            case "/":
            case "÷":
                result = values[0];
                for (int i = 1; i < values.length; i++){
                    result /= values[i];
                }
                break;
            default:
                // this shouldn't ever happen, but i wanted to write a switch statement
                result = -1;
        }

        return result;
    }

}
