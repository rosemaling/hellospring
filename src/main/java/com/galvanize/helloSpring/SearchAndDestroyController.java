package com.galvanize.helloSpring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchAndDestroyController {

    @GetMapping("/searchanddestroy")
    public String searchAndDestroy(@RequestParam String phrase, @RequestParam String search, @RequestParam String destroy){
        int startIndex = phrase.indexOf(search);
        int endIndex = startIndex + search.length();
        String builtString = "";

        if (startIndex == -1) {
            startIndex = phrase.length();
        }

        for(int i = 0; i < phrase.length(); i++) {
            if (i == startIndex) {
                builtString += destroy;
            } else if (i < startIndex || i > endIndex) {
                builtString += phrase.charAt(i);
            }
        }

        return builtString;
    }

}
