package com.galvanize.helloSpring;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(HelloController.class)
public class HelloControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void sayHello_withoutParams_rtnHelloWorld() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/hello")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello World"));

    }

    @Test
    void sayHello_withName_rtnHelloWorld() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/hello?name=Amanda")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello Amanda"));

    }

}
