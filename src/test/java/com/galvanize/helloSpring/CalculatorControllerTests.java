package com.galvanize.helloSpring;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(CalculatorController.class)
public class CalculatorControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void calculator_numberSeriesAndAdditionOperator_rtnComputedValue() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/calculator?values=1,2,3,4&operand=+")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("10"));

    }

    @Test
    void calculator_numberSeriesAndSubtractionOperator_rtnComputedValue() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/calculator?values=10,8,1,0,0&operand=-")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("1"));

    }

    @Test
    void calculator_numberSeriesAndMultiplicationOperator_rtnComputedValue() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/calculator?values=2,4,2&operand=*")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("16"));

    }

    @Test
    void calculator_numberSeriesAndDivisionOperator_rtnComputedValue() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/calculator?values=20,2,5&operand=/")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2"));

    }

    @Test
    void calculator_numberSeriesAndUnusualDivisionOperator_rtnComputedValue() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/calculator?values=10,5&operand=÷")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2"));

    }

}
