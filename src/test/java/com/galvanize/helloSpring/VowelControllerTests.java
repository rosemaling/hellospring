package com.galvanize.helloSpring;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(VowelController.class)
public class VowelControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void vowelCounter_wordWithValues_rtnVowelCount() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/vowels?word=jigsaw")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2"));

    }

}
