package com.galvanize.helloSpring;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(SearchAndDestroyController.class)
public class SearchAndDestroyControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void searchAndDestroy_withStringAndSubstring_rtnReplacedString() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/searchanddestroy?phrase=No excuses&search=excuses&destroy=equivocations")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("No equivocations"));

    }

    @Test
    void searchAndDestroy_withStringAndStringNotFound_rtnOriginalString() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/searchanddestroy?phrase=No excuses&search=equivocations&destroy=crying")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("No excuses"));

    }

}
